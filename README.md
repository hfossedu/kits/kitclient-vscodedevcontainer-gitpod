# KitClient-VSCodeDevContainer-GitPod

This project provides a Web accessible KitClient based on VS Code.
A KitClient is a pre-configured development environment suitable for
interacting with Kits.

## Requirements

1. A free GitLab account
2. A free GitPod account

## Start a new KitClient on GitPod

1. Open this project in GitPod.
    1. Navigate to https://gitpod.com/
    2. Sign in
    3. Select Dashboard
    4. Select New Workspace
    5. Paste URL to this project for the repository
    6. Select "VS Code - Browser" for editor.
    7. Select continue
2. Enter your name when the KitClient prompts you.

## Stop the KitClient

1. Open menu -> view -> command pallet
2. Select `GitPod: Stop Workspace`

## Restart the KitClient

1. Open existing workspace from GitPod.
    1. Navigate to https://gitpod.com/
    2. Sign in
    3. Select Dashboard
    4. Find the workspace and click on it.
2. Enter your name when the KitClient prompts you.
